# SingularityCompressor

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=mipicard_singularitycompressor&metric=alert_status)](https://sonarcloud.io/dashboard?id=mipicard_singularitycompressor)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=mipicard_singularitycompressor&metric=coverage)](https://sonarcloud.io/dashboard?id=mipicard_singularitycompressor)

## Application

This application should support at least 3 compression algorithm, respectively develop by a collaborator :

- Huffman (by Valentin Petit)
- LempelZivWelch (by Michaël Picard)
- RLE (by Michaël Picard)

## How to

Compile :
```bash
mvn package
```

Use :
```bash
java -jar target/SingularityCompressor.jar -h # For help
java -jar target/SingularityCompressor.jar --algorithm RLE --input inputFile --output outputFile # Compress inputFile with RLE and write in outputFile
java -jar target/SingularityCompressor.jar --algorithm RLE --input inputFile --output outputFile --uncompress # Uncompress inputFile with RLE and write in outputFile
```

## How do we worked

We firstly tried to develop the base of this project, with a simple algorithm (RLE).
We then pick an algorithm for each of us, and add unit testing, static analysis (SonarQube & GitLab SAST) and mutating-analysis (PIT).
We tried to reflect on them before merging with the master branch.

## Difficulties

Michaël :

    The most difficult part in this project was trying to understand how to integrate each component for the CI.
    It was very bothersome to implement and have made me do a lot of "useless" commit, to see if what I've worked will 
    work, or not. But in the end, most of all the wanted tools have been used, even though it was hardly automated. The
    only automated try was with PMD in its own branch, but the job wasn't failed as expected, so we haven't merge it
    with the master branch.
    
Valentin :

    I chose to implement the Huffman algorithm because I know it really well, but I did not realize how hard it
    could be, for me, to implement it using IntpuStream and OutputStream, and testing it.
    I'm well aware of the advantages of unit testing, but i'm not confident enough with input/output stream to completely
    test the huffman Algorithm.
    Moreover, I badly managed my time during the last month, I was most of the time working for the company during my free time
    instead of working on this project. That's why we chose not to merge my branch into Master, so that the integration test would
    not fail.