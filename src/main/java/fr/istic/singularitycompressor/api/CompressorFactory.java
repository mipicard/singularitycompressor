package fr.istic.singularitycompressor.api;

import fr.istic.singularitycompressor.impl.LempelZivWelchCompressor;
import fr.istic.singularitycompressor.impl.RunLengthEncodingCompressor;

import java.lang.reflect.InvocationTargetException;

public class CompressorFactory {
    public enum Algorithm {
        Huffman(null),
        LempelZivWelch(LempelZivWelchCompressor.class),
        RLE(RunLengthEncodingCompressor.class);

        private Class<? extends Compressor> algorithmClass;

        Algorithm(Class<? extends Compressor> algorithmClass) {
            this.algorithmClass = algorithmClass;
        }

        public Class<? extends Compressor> getAlgorithmClass() {
            return algorithmClass;
        }
    }

    public static Compressor fromString(String algorithm) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return build(Algorithm.valueOf(algorithm));
    }

    public static Compressor build(Algorithm algorithm) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return algorithm.getAlgorithmClass().getDeclaredConstructor().newInstance();
    }
}
