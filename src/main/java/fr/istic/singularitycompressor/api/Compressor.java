package fr.istic.singularitycompressor.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Compressor {
    /**
     * Compress the inputted file and save the result in the outputted file.
     * @param input
     * @param output
     */
    void compress(InputStream input, OutputStream output) throws IOException;

    /**
     * Uncompress the inputted file and save the result in the outputted file.
     * @param input
     * @param output
     */
    void uncompress(InputStream input, OutputStream output) throws IOException;
}
