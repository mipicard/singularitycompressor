package fr.istic.singularitycompressor;

import fr.istic.singularitycompressor.api.Compressor;
import fr.istic.singularitycompressor.api.CompressorFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {
        CommandLineInterface arguments = CommandLineInterface.parse(args);
        InputStream input;
        OutputStream output;

        if(StringUtils.isNotBlank(arguments.getInput()))
            input = new FileInputStream(arguments.getInput());
        else
            input = System.in;

        if(StringUtils.isNotBlank(arguments.getOutput()))
            output = new FileOutputStream(arguments.getOutput());
        else
            output = System.out;

        Compressor compressor = CompressorFactory.fromString(arguments.getAlgorithm());

        if(arguments.isUncompress())
            compressor.uncompress(input, output);
        else
            compressor.compress(input, output);

        input.close();
        output.close();
    }
}
