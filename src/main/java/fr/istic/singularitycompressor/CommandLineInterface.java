package fr.istic.singularitycompressor;

import picocli.CommandLine;
import picocli.CommandLine.Option;

public class CommandLineInterface {

    @Option(names = {"-i", "--input"}, description = "Use the given file as input.")
    private String input;

    @Option(names = {"-o", "--output"}, description = "Use the given file as output.")
    private String output;

    @Option(names = {"-a", "--algorithm"}, required = true, description = "The algorithm to use, between : Huffman, LempelZivWelch and RLE.")
    private String algorithm;

    @Option(names = {"-u", "--uncompress"}, description = "Uncompress the file instead.")
    private boolean uncompress;

    @Option(names = {"-h", "--help"}, usageHelp = true, description = "Show this message.")
    private boolean help;

    public String getInput() {
        return input;
    }

    public String getOutput() {
        return output;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public boolean isUncompress() {
        return uncompress;
    }

    public boolean isHelp() {
        return help;
    }

    public static CommandLineInterface parse(String[] args) {
        CommandLineInterface arguments = CommandLine.populateCommand(new CommandLineInterface(), args);
        if(arguments.isHelp()) {
            CommandLine.usage(arguments, System.out);
            System.exit(0);
        }
        return arguments;
    }
}
