package fr.istic.singularitycompressor.impl;

import fr.istic.singularitycompressor.api.Compressor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class RunLengthEncodingCompressor implements Compressor {

    @Override
    public void compress(InputStream input, OutputStream output) throws IOException {
        byte[] dataInput = input.readAllBytes();
        List<Byte> dataOutput = new LinkedList<>();
        if(dataInput.length > 0) {
            byte last = dataInput[0];
            byte count = 1;
            for(int i = 1; i < dataInput.length; ++i) {
                if(dataInput[i] == last && count != -1) {
                    // If we have a 256+ sequence of the same character, break it
                    ++count;
                } else {
                    dataOutput.add(count);
                    dataOutput.add(last);
                    last = dataInput[i];
                    count = 1;
                }
            }
            dataOutput.add(count);
            dataOutput.add(last);
            output.write(Utils.byteListToByteArray(dataOutput));
        }
    }

    @Override
    public void uncompress(InputStream input, OutputStream output) throws IOException {
        byte[] dataInput = input.readAllBytes();
        List<Byte> dataOutput = new LinkedList<>();
        if(dataInput.length % 2 != 0)
            throw new IllegalArgumentException("Data can't be uncompressed. Wrong length.");
        if(dataInput.length > 0) {
            for(int i = 0; i < dataInput.length; i+=2) {
                for(byte c = 0; c < dataInput[i]; ++c) {
                    dataOutput.add(dataInput[i+1]);
                }
            }
            output.write(Utils.byteListToByteArray(dataOutput));
        }
    }
}
