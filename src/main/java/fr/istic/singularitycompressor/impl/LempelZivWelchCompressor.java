package fr.istic.singularitycompressor.impl;

import fr.istic.singularitycompressor.api.Compressor;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.*;

public class LempelZivWelchCompressor implements Compressor {
    private static final int BYTE_LENGTH = 8;

    @Override
    public void compress (InputStream input, OutputStream output) throws IOException {
        List<String> dataStringInput = byteArrayToBinaryStringList(input.readAllBytes());
        List<BigInteger> dataOutput = new LinkedList<>();
        BigInteger dictSize = BigInteger.valueOf(256);
        Map<String, BigInteger> dictionary = new HashMap<>();

        byte b = 0;
        for(BigInteger i = BigInteger.ZERO; i.compareTo(dictSize) < 0; i = i.add(BigInteger.ONE)) {
            dictionary.put(byteToBinaryString(b), i);
            b++;
        }

        String w = "";
        for(String c : dataStringInput) {
            String wc = w + c;
            if(dictionary.containsKey(wc)) {
                w = wc;
            } else {
                dataOutput.add(dictionary.get(w));
                dictionary.put(wc, dictSize);
                dictSize = dictSize.add(BigInteger.ONE);
                w = c;
            }
        }
        if(StringUtils.isNotBlank(w))
            dataOutput.add(dictionary.get(w));

        output.write(bigIntegerListToByteArrayWithSize(dataOutput));
    }

    @Override
    public void uncompress (InputStream input, OutputStream output) throws IOException {
        List<BigInteger> dataInput = byteArrayToBigIntegerListWithSize(input.readAllBytes());
        BigInteger dictSize = BigInteger.valueOf(256);
        Map<BigInteger, String> dictionary = new HashMap<>();

        if(dictSize.compareTo(dataInput.get(0)) <= 0)
            throw new IllegalArgumentException("Bad compressed first character: " + dataInput.get(0));

        byte b = 0;
        for(BigInteger i = BigInteger.ZERO; i.compareTo(dictSize) < 0; i = i.add(BigInteger.ONE)) {
            dictionary.put(i, byteToBinaryString(b));
            b++;
        }

        String w = dictionary.get(dataInput.remove(0));
        List<Byte> dataOutput = new LinkedList<>(binaryStringToByteList(w));
        for(BigInteger k : dataInput) {
            String entry;
            if(dictionary.containsKey(k))
                entry = dictionary.get(k);
            else if (k.equals(dictSize))
                entry = w + w.substring(0, BYTE_LENGTH);
            else
                throw new IllegalArgumentException("Bad compressed k: " + k);

            dataOutput.addAll(binaryStringToByteList(entry));

            dictionary.put(dictSize, w + entry.substring(0, BYTE_LENGTH));
            dictSize = dictSize.add(BigInteger.ONE);

            w = entry;
        }

        output.write(Utils.byteListToByteArray(dataOutput));
    }

    /**
     * Convert a byte in a binary String.
     * @param b the byte
     * @return a binary String
     */
    String byteToBinaryString (byte b) {
        StringBuilder result = new StringBuilder();
        int buffer = b & 0xFF;
        for(int i = 0; i < BYTE_LENGTH; ++i) {
            result.insert(0, buffer % 2 != 0 ? "1" : "0");
            buffer /= 2;
        }
        return result.toString();
    }

    /**
     * Find the max BigInteger in the given list
     * @param list BigInteger list
     * @return the max BigInteger
     * @throws IllegalArgumentException if the list is empty or null
     */
    BigInteger max (List<BigInteger> list) {
        if(list == null || list.isEmpty())
            throw new IllegalArgumentException("Null or empty list.");

        BigInteger max = list.get(0);

        for(int i = 1; i < list.size(); ++i)
            max = max.compareTo(list.get(i)) >= 0 ? max : list.get(i);

        return max;
    }

    /**
     * Determine how many time you have to shift in order to fit in the leftOver.
     *
     * For example, you have a byte with a leftOver of 4, bit not used by the preceding value.
     * You have to fit your value such that every leftOver bit will be used. Depending of the exceed, the number of
     * highest bit that are efficiently used in your value, you will shift left your number, producing a new leftOver
     * equal to number of shift you have done, available for the next value.
     * XXXX0000 -> leftOver of 4 ; XXXXXX00 11001100 -> your value (X = 0 in the value)
     * => you shift by 2
     * XXXX0011 001100XX -> your shifted value (X = 0 in the value), with a new leftOver of 2
     *
     * @param leftOver the available space
     * @param exceed the highest efficiently used bits
     * @return the number of shift left you have to do.
     */
    int shiftLeft (int leftOver, int exceed) {
        int shift;
        if(leftOver == 0 && leftOver != exceed)
            shift = BYTE_LENGTH - exceed;
        else if (leftOver < exceed)
            shift = leftOver + BYTE_LENGTH - exceed;
        else
            shift = leftOver - exceed;
        return shift;
    }

    /**
     * Convert a byte array to the corresponding list of binary String
     * @param data the bite array
     * @return a list of binary String
     */
    List<String> byteArrayToBinaryStringList (byte[] data) {
        List<String> result = new LinkedList<>();
        for (byte b : data)
            result.add(byteToBinaryString(b));
        return result;
    }

    /**
     * Convert a binaryString of multiple byte representation in a list of Byte
     * @param binaryString the binary String
     * @return a list of Byte
     * @throws IllegalArgumentException If the String is not parsable as a list of Byte.
     */
    List<Byte> binaryStringToByteList (String binaryString) {
        String data = binaryString.strip();
        if(data.length() % BYTE_LENGTH != 0)
            throw new IllegalArgumentException("\"" +binaryString + "\" is not bynary string parsable in Byte list.");
        final int length = data.length() / BYTE_LENGTH;
        List<Byte> byteList = new ArrayList<>(length);
        try {
            for (int i = 0; i < length; ++i)
                byteList.add((byte) Integer.parseInt(data.substring(i * BYTE_LENGTH, (i + 1) * BYTE_LENGTH), 2));
            return byteList;
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("\"" +binaryString + "\" is not bynary string parsable in Byte list.", nfe);
        }
    }

    /**
     * Convert a list of BigInteger in a byte array, with the size of an encoded binary value of a BigInteger in the first byte.
     * @param data the list of BigIntger
     * @return a byte array
     * @throws IllegalArgumentException If the list is empty or null.
     */
    byte[] bigIntegerListToByteArrayWithSize(List<BigInteger> data) {
        if(data == null || data.isEmpty())
            throw new IllegalArgumentException("Null or empty list.");

        // Set the minimum length at 8. Ideal to ignore trailing 0 who can be understand as character when doing the inverse operation.
        final int charLength = Math.max(max(data).bitLength(), 8);
        final int arrayLength = 1 + data.size()*charLength/BYTE_LENGTH + (data.size()*charLength%BYTE_LENGTH > 0 ? 1 : 0);
        final byte[] byteArray = new byte[arrayLength];
        byteArray[0] = (byte) charLength;

        final int exceed = charLength % BYTE_LENGTH;
        int leftOver = 0;
        int indexByteArray = 1;
        for(BigInteger character : data) {
            // Normalize
            int shift = shiftLeft(leftOver, exceed);

            // Insert
            int numberOfByte = (charLength+shift)/BYTE_LENGTH + ((charLength+shift)%BYTE_LENGTH != 0 ? 1 : 0);
            byte[] toAdd = bigIntegerToByteArray(character.shiftLeft(shift),numberOfByte);
            for(int i = 0; i < toAdd.length; ++i)
                byteArray[indexByteArray++] += toAdd[i];

            // Next leftover for next value
            leftOver = shift;
            if(leftOver != 0)// Correction of indexByteArray
                indexByteArray--;
        }
        return byteArray;
    }

    /**
     * Convert a BigInteger in his byte array representation, following the numberOfByte requested.
     * @param value the BigInteger
     * @param numberOfByte the numberOfByte
     * @return a byte array
     */
    byte[] bigIntegerToByteArray(BigInteger value, int numberOfByte) {
        byte[] valueArray = value.toByteArray();
        int difference = valueArray.length - numberOfByte;
        if(difference == 0) {
            return valueArray;
        } else if(difference > 0) { // valueArray.lengh > numberOfByte
            byte[] result = new byte[numberOfByte];
            System.arraycopy(valueArray, difference, result, 0, numberOfByte);
            return result;
        } else { // valueArray.lengh < numberOfByte
            byte[] result = new byte[numberOfByte];
            System.arraycopy(valueArray, 0, result, 0 - difference, valueArray.length);
            return result;
        }
    }

    /**
     * Convert an unsigned byte array to a BigInteger
     * @param byteArray the unsigned byte array
     * @return a BigInteger
     */
    BigInteger unsignedByteArrayToBigInteger(byte[] byteArray) {
        if(byteArray[0] >= 0) {
            return new BigInteger(byteArray);
        } else {
            byte[] buffer = new byte[byteArray.length + 1];
            System.arraycopy(byteArray, 0, buffer, 1, byteArray.length);
            return new BigInteger(buffer);
        }
    }

    /**
     * Convert a byte array in a list of BigInteger, with the size of an encoded binary value of a BigInteger in the first byte.
     * @param data the byte array
     * @return a list of BigInteger
     * @throws IllegalArgumentException If the list is empty or null, or if data isn't well formatted.
     */
    List<BigInteger> byteArrayToBigIntegerListWithSize(byte[] data) {
        if(data == null || data.length == 0) // Dummy array
            throw new IllegalArgumentException("Null or empty list.");
        if(data.length == 1) // Array with only size
            throw new IllegalArgumentException("Data can't be uncompressed, no data found.");

        final int charLength = Byte.toUnsignedInt(data[0]);
        if(charLength == 0)
            throw new IllegalArgumentException("Data can't be uncompressed, character size can't be 0.");

        final int listLength = (data.length - 1)*BYTE_LENGTH/charLength;
        final int lastValueTrailingLength = (data.length - 1)*BYTE_LENGTH%charLength;

        if(lastValueTrailingLength != 0 && (byte)(data[data.length - 1] << (BYTE_LENGTH - lastValueTrailingLength)) != 0) // don't end with 0 bits.
            throw new IllegalArgumentException("Data can't be uncompressed, ending file is corrupted.");

        List<BigInteger> bigIntegerList = new ArrayList<>(listLength);
        final int exceed = charLength % BYTE_LENGTH;
        int leftover = 0;
        int indexByteArray = 1;
        for(int index = 0; index < listLength; ++index) {
            // Normalize
            int shift = shiftLeft(leftover, exceed);

            // Retrieve byte array
            int lengthCharacterArray = (charLength + (BYTE_LENGTH - leftover) % BYTE_LENGTH + shift) / BYTE_LENGTH;
            byte[] character = new byte[lengthCharacterArray];
            for(int i = 0; i < lengthCharacterArray; ++i)
                character[i] = data[indexByteArray++];

            // Clean data
            BigInteger toAdd = unsignedByteArrayToBigInteger(character);
            for(int i = 0; i < BYTE_LENGTH - leftover; i++)
                toAdd = toAdd.clearBit(charLength + shift + i);
            toAdd = toAdd.shiftRight(shift);
            bigIntegerList.add(toAdd);

            // Next leftover for next value
            leftover = shift;
            if(leftover != 0)// Correction of indexByteArray
                indexByteArray--;
        }

        return bigIntegerList;
    }
}
