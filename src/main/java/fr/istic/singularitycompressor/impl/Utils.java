package fr.istic.singularitycompressor.impl;

import java.util.List;

class Utils {

    private Utils() {} // Prevent instantiation

    /**
     * Convert a list of Byte to a byte array.
     * @param bytes the list of Byte
     * @return a byte array
     */
    static byte[] byteListToByteArray (List<Byte> bytes) {
        byte[] out = new byte[bytes.size()];
        for(int i = 0; i < bytes.size(); ++i) {
            out[i] = bytes.get(i);
        }
        return out;
    }
}
