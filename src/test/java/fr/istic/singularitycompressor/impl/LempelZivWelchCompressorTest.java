package fr.istic.singularitycompressor.impl;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class LempelZivWelchCompressorTest {

    private InputStream inputMock;
    private OutputStream outputMock;
    private LempelZivWelchCompressor compressor;

    @BeforeEach
    public void beforeEach () {
        inputMock = EasyMock.mock(InputStream.class);
        outputMock = EasyMock.mock(OutputStream.class);
        compressor = new LempelZivWelchCompressor();
    }

    // byteToBinaryString

    @Test
    public void byteToBinaryString () {
        assertEquals("11111111", compressor.byteToBinaryString((byte)255));
        assertEquals("00000000", compressor.byteToBinaryString((byte)0));
        assertEquals("10000010", compressor.byteToBinaryString((byte)130));
        assertEquals("01000110", compressor.byteToBinaryString((byte)70));
    }

    // max

    @Test
    public void max () {
        List<BigInteger> values = List.of(BigInteger.ONE, BigInteger.ZERO, BigInteger.ONE, BigInteger.TWO);
        BigInteger expected = BigInteger.TWO;

        assertEquals(expected, compressor.max(values));
    }

    @Test
    public void max_EmptyOrNull () {
        assertThrows(IllegalArgumentException.class, () -> compressor.max(null));
        assertThrows(IllegalArgumentException.class, () -> compressor.max(List.of()));
    }

    // shiftLeft

    @Test
    public void shiftLeft_sameLeftOverAndExceed () {
        int left_over = 2;
        int exceed = 2;
        int expected_shift = 0;

        assertEquals(expected_shift, compressor.shiftLeft(left_over, exceed));
    }

    @Test
    public void shiftLeft_exceedLessOrEqualLeftOver () {
        int left_over = 4;
        int exceed = 2;
        int expected_shift = 2;

        assertEquals(expected_shift, compressor.shiftLeft(left_over, exceed));
    }

    @Test
    public void shiftLeft_exceedMoreThanLeftOver () {
        int left_over = 4;
        int exceed = 6;
        int expected_shift = 6;

        assertEquals(expected_shift, compressor.shiftLeft(left_over, exceed));
    }

    @Test
    public void shiftLeft_leftOverIsZero () {
        int left_over = 0;
        int exceed = 6;
        int expected_shift = 2;

        assertEquals(expected_shift, compressor.shiftLeft(left_over, exceed));
    }

    @Test
    public void shiftLeft_leftOverIsZeroAndEqualToExceed () {
        int left_over = 0;
        int exceed = 0;
        int expected_shift = 0;

        assertEquals(expected_shift, compressor.shiftLeft(left_over, exceed));
    }

    // byteArrayToBinaryStringList

    @Test
    public void byteArrayToBinaryStringList_emptyByteArray () {
        byte[] data = new byte[]{};
        List<String> expected = List.of();

        assertEquals(expected, compressor.byteArrayToBinaryStringList(data));
    }

    @Test
    public void byteArrayToBinaryStringList () {
        byte[] data = new byte[]{1, 2, 4, 8, 16, 32, 64, -128, 42};
        List<String> expected = List.of("00000001", "00000010", "00000100", "00001000", "00010000", "00100000", "01000000", "10000000", "00101010");

        assertEquals(expected, compressor.byteArrayToBinaryStringList(data));
    }

    // binaryStringToByteList

    @Test
    public void binaryStringToByteList_emptyStringOrBlank () {
        String empty = "";
        String blank = "      ";
        List<Byte> expected = List.of();

        assertEquals(expected, compressor.binaryStringToByteList(empty));
        assertEquals(expected, compressor.binaryStringToByteList(blank));
    }

    @Test
    public void binaryStringToByteList_wrongLength () {
        String binaryString = "01110";

        assertThrows(IllegalArgumentException.class,() -> compressor.binaryStringToByteList(binaryString));
    }

    @Test
    public void binaryStringToByteList_notBinaryString () {
        String binaryString = "hello!!!";

        assertThrows(IllegalArgumentException.class,() -> compressor.binaryStringToByteList(binaryString));
    }

    @Test
    public void binaryStringToByteList () {
        String binaryString = "00000001000000100000010010000000";
        List<Byte> expected = List.of((byte) 1, (byte) 2, (byte) 4, (byte) -128);

        assertEquals(expected, compressor.binaryStringToByteList(binaryString));
    }

    // bigIntegerListToByteArrayWithSize

    @Test
    public void bigIntegerListToByteArrayWithSize_emptyOrNull () {
        List<BigInteger> empty = List.of();

        assertThrows(IllegalArgumentException.class, () -> compressor.bigIntegerListToByteArrayWithSize(empty));
        assertThrows(IllegalArgumentException.class, () -> compressor.bigIntegerListToByteArrayWithSize(null));
    }

    @Test
    public void bigIntegerListToByteArrayWithSize_charLength9 () {
        List<BigInteger> data = List.of(BigInteger.ZERO, BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(4), BigInteger.valueOf(8),
                BigInteger.valueOf(16), BigInteger.valueOf(32), BigInteger.valueOf(64), BigInteger.valueOf(257));
        byte[] expected = new byte[]{9, 0, 0, 64, 64, 64, 64, 64, 64, 64, -128, -128};

        assertArrayEquals(expected, compressor.bigIntegerListToByteArrayWithSize(data));
    }

    @Test
    public void bigIntegerListToByteArrayWithSize_charLength8 () {
        List<BigInteger> data = List.of(BigInteger.ZERO, BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(4), BigInteger.valueOf(8),
                BigInteger.valueOf(16), BigInteger.valueOf(32), BigInteger.valueOf(64), BigInteger.valueOf(128));
        byte[] expected = new byte[]{8, 0, 1, 2, 4, 8, 16, 32, 64, -128};

        assertArrayEquals(expected, compressor.bigIntegerListToByteArrayWithSize(data));
    }

    // bigIntegerToByteArray

    @Test
    public void bigIntegerToByteArray_NumberOfByteIsZero () {
        BigInteger value = BigInteger.valueOf(42);
        int numberOfByte = 0;
        byte[] expected = new byte[]{};

        assertArrayEquals(expected, compressor.bigIntegerToByteArray(value, numberOfByte));
    }

    @Test
    public void bigIntegerToByteArray_NumberOfByteIsOverValue () {
        BigInteger value = BigInteger.valueOf(42);
        int numberOfByte = 2;
        byte[] expected = new byte[]{0, 42};

        assertArrayEquals(expected, compressor.bigIntegerToByteArray(value, numberOfByte));
    }

    @Test
    public void bigIntegerToByteArray_NumberOfByteIsUnderValue () {
        BigInteger value = BigInteger.valueOf(512);
        int numberOfByte = 1;
        byte[] expected = new byte[]{0};

        assertArrayEquals(expected, compressor.bigIntegerToByteArray(value, numberOfByte));
    }

    @Test
    public void bigIntegerToByteArray_NumberOfByteIsEqualToUnsignedValue () {
        BigInteger value = BigInteger.valueOf(255);
        int numberOfByte = 1;
        byte[] expected = new byte[]{-1};

        assertArrayEquals(expected, compressor.bigIntegerToByteArray(value, numberOfByte));
    }

    // unsignedByteArrayToBigInteger

    @Test
    public void unsignedByteToBigInteger_positiveFirstByte () {
        byte[] array = new byte[]{127};
        BigInteger expected = BigInteger.valueOf(127);

        assertEquals(expected, compressor.unsignedByteArrayToBigInteger(array));
    }

    @Test
    public void unsignedByteToBigInteger_negativeFirstByte () {
        byte[] array = new byte[]{-128};
        BigInteger expected = BigInteger.valueOf(128);

        assertEquals(expected, compressor.unsignedByteArrayToBigInteger(array));
    }

    // byteArrayToBigIntegerListWithSize

    @Test
    public void byteArrayToBigIntegerListWithSize_emptyOrNull () {
        byte[] empty = new byte[]{};

        assertThrows(IllegalArgumentException.class, () -> compressor.byteArrayToBigIntegerListWithSize(empty));
        assertThrows(IllegalArgumentException.class, () -> compressor.byteArrayToBigIntegerListWithSize(null));
    }

    @Test
    public void byteArrayToBigIntegerListWithSize_onlySize () {
        byte[] data_size3 = new byte[]{3};

        assertThrows(IllegalArgumentException.class, () -> compressor.byteArrayToBigIntegerListWithSize(data_size3));
    }

    @Test
    public void byteArrayToBigIntegerListWithSize_sizeNull () {
        byte[] data_size0 = new byte[]{0, 1, 1, 1};

        assertThrows(IllegalArgumentException.class, () -> compressor.byteArrayToBigIntegerListWithSize(data_size0));
    }

    @Test
    public void byteArrayToBigIntegerListWithSize_wrongEncoded () {
        byte[] data = new byte[]{3, 1};

        assertThrows(IllegalArgumentException.class, () -> compressor.byteArrayToBigIntegerListWithSize(data));
    }

    @Test
    public void byteArrayToBigIntegerListWithSize_charLength9 () {
        byte[] data = new byte[]{9, 0, 0, 64, 64, 64, 64, 64, 64, 64, -128, -128};
        List<BigInteger> expected = List.of(BigInteger.ZERO, BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(4), BigInteger.valueOf(8),
                BigInteger.valueOf(16), BigInteger.valueOf(32), BigInteger.valueOf(64), BigInteger.valueOf(257));

        assertEquals(expected, compressor.byteArrayToBigIntegerListWithSize(data));
    }

    @Test
    public void byteArrayToBigIntegerListWithSize_charLength8 () {
        byte[] data = new byte[]{8, 0, 1, 2, 4, 8, 16, 32, 64, -128};
        List<BigInteger> expected = List.of(BigInteger.ZERO, BigInteger.ONE, BigInteger.TWO, BigInteger.valueOf(4), BigInteger.valueOf(8),
                BigInteger.valueOf(16), BigInteger.valueOf(32), BigInteger.valueOf(64), BigInteger.valueOf(128));

        assertEquals(expected, compressor.byteArrayToBigIntegerListWithSize(data));
    }

    // API

    @Test
    public void compress() throws IOException {
        // Prepare
        byte[] inputData = new byte[]{'T', 'O', 'B', 'E', 'O', 'R', 'N', 'O', 'T', 'T', 'O', 'B', 'E', 'O', 'R', 'T', 'O', 'B', 'E', 'O', 'R', 'N', 'O', 'T'};
        byte[] expectedOutput = new byte[] {9, 42, 19, -56, 68, 82, 121, 72, -100, 79, 42, 64, 32, 80, 72, 76, 14, 11, 7};

        // Mock
        EasyMock.expect(inputMock.readAllBytes()).andReturn(inputData);
        /* expect */ outputMock.write(expectedOutput);
        EasyMock.expectLastCall().once();
        EasyMock.replay(inputMock, outputMock);

        // When
        compressor.compress(inputMock, outputMock);

        // Assert
        EasyMock.verify(inputMock, outputMock);
    }

    @Test
    public void uncompress() throws IOException {
        // Prepare
        byte[] inputData = new byte[] {9, 42, 19, -56, 68, 82, 121, 72, -100, 79, 42, 64, 32, 80, 72, 76, 14, 11, 7};
        byte[] expectedOutput = new byte[]{'T', 'O', 'B', 'E', 'O', 'R', 'N', 'O', 'T', 'T', 'O', 'B', 'E', 'O', 'R', 'T', 'O', 'B', 'E', 'O', 'R', 'N', 'O', 'T'};

        // Mock
        EasyMock.expect(inputMock.readAllBytes()).andReturn(inputData);
        /* expect */ outputMock.write(expectedOutput);
        EasyMock.expectLastCall().once();
        EasyMock.replay(inputMock, outputMock);

        // When
        compressor.uncompress(inputMock, outputMock);

        // Assert
        EasyMock.verify(inputMock, outputMock);
    }
}
