package fr.istic.singularitycompressor.impl;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class RunLengthEncodingCompressorTest {

    private InputStream inputMock;
    private OutputStream outputMock;
    private RunLengthEncodingCompressor compressor;

    @BeforeEach
    public void setUp() {
        inputMock = EasyMock.mock(InputStream.class);
        outputMock = EasyMock.mock(OutputStream.class);
        compressor = new RunLengthEncodingCompressor();
    }

    @Test
    public void compress() throws IOException {
        // Prepare
        byte[] inputData = new byte[]{'t', 'e', 'e', 's', 't', 't', 'i', 'n', 'g'};
        byte[] expectedOutput = new byte[] {1, 't', 2, 'e', 1, 's', 2, 't', 1, 'i', 1, 'n', 1, 'g'};

        // Mock
        EasyMock.expect(inputMock.readAllBytes()).andReturn(inputData);
        /* expect */ outputMock.write(expectedOutput);
        EasyMock.expectLastCall().once();
        EasyMock.replay(inputMock, outputMock);

        // When
        compressor.compress(inputMock, outputMock);

        // Assert
        EasyMock.verify(inputMock, outputMock);
    }

    @Test
    public void compress_hugeSequence() throws IOException {
        // Prepare
        byte[] inputData = new byte[]{'a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a','a', 'a', 'a', 'a', 'a'};
        // inputData = sequence of 320 'a'
        byte[] expectedOutput = new byte[] {-1, 'a', 65, 'a'};

        // Mock
        EasyMock.expect(inputMock.readAllBytes()).andReturn(inputData);
        /* expect */ outputMock.write(expectedOutput);
        EasyMock.expectLastCall().once();
        EasyMock.replay(inputMock, outputMock);

        // When
        compressor.compress(inputMock, outputMock);

        // Assert
        EasyMock.verify(inputMock, outputMock);
    }

    @Test
    public void uncompres() throws IOException {
        // Prepare
        byte[] inputData = new byte[] {1, 't', 2, 'e', 1, 's', 2, 't', 1, 'i', 1, 'n', 1, 'g'};
        byte[] expectedOutput = new byte[]{'t', 'e', 'e', 's', 't', 't', 'i', 'n', 'g'};

        // Mock
        EasyMock.expect(inputMock.readAllBytes()).andReturn(inputData);
        /* expect */ outputMock.write(expectedOutput);
        EasyMock.expectLastCall().once();
        EasyMock.replay(inputMock, outputMock);

        // When
        compressor.uncompress(inputMock, outputMock);

        // Assert
        EasyMock.verify(inputMock, outputMock);
    }

    @Test
    public void uncompress_IAE() throws IOException {
        // Prepare
        byte[] inputData = new byte[] {1, 't', 2, 'e', 1, 's', 2, 't', 1, 'i', 1, 'n', 1};

        // Mock
        EasyMock.expect(inputMock.readAllBytes()).andReturn(inputData);
        EasyMock.replay(inputMock);

        // When
        assertThrows(IllegalArgumentException.class, () -> compressor.uncompress(inputMock, outputMock), "Data can't be uncompressed. Wrong length.");
    }
}
