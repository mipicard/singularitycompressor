package fr.istic.singularitycompressor.impl;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class UtilsTest {

    // byteListToByteArray

    @Test
    public void byteListToByteArray_emptyByteList () {
        List<Byte> data = List.of();
        byte[] expected = new byte[]{};

        assertArrayEquals(expected, Utils.byteListToByteArray(data));
    }

    @Test
    public void byteListToByteArray () {
        List<Byte> data = List.of((byte) 1, (byte) 2, (byte) 4, (byte) 8);
        byte[] expected = new byte[]{1, 2, 4, 8};

        assertArrayEquals(expected, Utils.byteListToByteArray(data));
    }
}
